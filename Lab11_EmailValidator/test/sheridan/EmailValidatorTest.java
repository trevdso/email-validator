package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testIsValidFormat() {
		boolean result = EmailValidator.isValidFormat("kjlasd@jso.com");
		assertTrue("Invalid format", result);
	}
	
	@Test
	public void testIsValidFormatException() {
		boolean result = EmailValidator.isValidFormat("kjlasdjsocom");
		assertFalse("Invalid format", result);
	}
	
	@Test
	public void testIsValidFormatBoundaryIn() {
		boolean result = EmailValidator.isValidFormat("kjlasd.jso@com");
		assertFalse("Invalid format", result);
	}
	
	@Test
	public void testIsValidFormatBoundaryOut() {
		boolean result = EmailValidator.isValidFormat("@kjlasdjsocom.");
		assertFalse("Invalid format", result);
	}
	
	//----------
	
	@Test
	public void testIsValidAtSymbol() {
		boolean result = EmailValidator.hasOnlyOneAtSymbol("kjlasd@jso.com");
		assertTrue("Invalid format", result);
	}
	
	@Test
	public void testIsValidAtSymbolException() {
		boolean result = EmailValidator.hasOnlyOneAtSymbol(" ");
		assertFalse("Invalid format", result);
	}
	
	@Test
	public void testIsValidAtSymbolBoundaryIn() {
		boolean result = EmailValidator.hasOnlyOneAtSymbol("kjlasdjso.com");
		assertFalse("Invalid format", result);
	}
	
	@Test
	
	public void testIsValidAtSymbolBoundaryOut() {
		boolean result = EmailValidator.hasOnlyOneAtSymbol("kjl@asd@jso.com");
		assertFalse("Invalid format", result);
	}
	
	//----------
	
	@Test
	public void testIsValidAccount() {
		boolean result = EmailValidator.isValidAccount("kjlas12d@jso.com");
		assertTrue("Invalid format", result);
	}
	
	@Test
	public void testIsValidAccountException() {
		boolean result = EmailValidator.isValidAccount("2FGH12d@jso.com");
		assertFalse("Invalid format", result);
	}
	
	@Test
	public void testIsValidAccountBoundaryIn() {
		boolean result = EmailValidator.isValidAccount("kd@jso.com");
		assertFalse("Invalid format", result);
	}
	
	@Test
	public void testIsValidAccountBoundaryOut() {
		boolean result = EmailValidator.isValidAccount("k2j3q4w5e312d@jso.com");
		assertTrue("Invalid format", result);
	}
	
	//--------
	
	@Test
	public void testIsValidDomain() {
		boolean result = EmailValidator.isValidDomain("kjlas12d@j21o.com");
		assertTrue("Invalid format", result);
	}
	
	@Test
	public void testIsValidDomainException() {
		boolean result = EmailValidator.isValidDomain("kjlas12d@.com");
		assertFalse("Invalid format", result);
	}
	
	@Test
	public void testIsValidDomainBoundaryIn() {
		boolean result = EmailValidator.isValidDomain("kjlas12d@1a123.com");
		assertTrue("Invalid format", result);
	}
	
	@Test
	public void testIsValidDomainBoundaryOut() {
		boolean result = EmailValidator.isValidDomain("kjlas12d@qwer123.com");
		assertTrue("Invalid format", result);
	}
	
	//----------
	
	@Test
	public void testIsValidExt() {
		boolean result = EmailValidator.isValidExtension("kjlasd@jso.com");
		assertTrue("Invalid format", result);
	}
	
	@Test
	public void testIsValidExtException() {
		boolean result = EmailValidator.isValidExtension("kjlasd@jso.");
		assertFalse("Invalid format", result);
	}
	
	@Test
	public void testIsValidExtBoundaryIn() {
		boolean result = EmailValidator.isValidExtension("kjlasd@jso.c");
		assertFalse("Invalid format", result);
	}
	
	@Test
	public void testIsValidExtBoundaryOut() {
		boolean result = EmailValidator.isValidExtension("kjlasd@jso.co2m");
		assertFalse("Invalid format", result);
	}

}
