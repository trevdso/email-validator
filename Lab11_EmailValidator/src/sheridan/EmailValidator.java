package sheridan;

import java.util.logging.Level;
import java.util.logging.Logger;

public class EmailValidator {
	
	static Logger logger = Logger.getLogger(EmailValidator.class.getName());
	
	public static void main(String args[]) {
		System.out.println("Email Validator Lab 11 1.0");
		String email = "dsoutrev@sheridancollege.ca";
		System.out.println("Email provided: " + email);
		System.out.println("Is valid email ? : " + isValidEmail(email));
	}

	public static boolean isValidEmail(String email) {
		if(isValidFormat(email) && hasOnlyOneAtSymbol(email) && isValidAccount(email) && isValidDomain(email) && isValidExtension(email)) {
			return true;
		}else {
			return false;
		}
	}
	
	/*
	 * format must have a <account>@<domain>.<extension>
	 */
	public static boolean isValidFormat(String email) {
		return email != null && email.matches("^(.+)@(.+)\\.(.+)$");
	}
	
	/*
	 * email must have only one @ symbol
	 */
	public static boolean hasOnlyOneAtSymbol(String email) {
		return email != null && email.matches("^[^@]*@[^@]*$");
	}
	
	/*
	 * account name should have at least 3 alpha-characters in lowercase (must not start with a number)
	 */
	public static boolean isValidAccount(String email) {
		if(email.contains("@")) {
			email = email.substring(0, email.indexOf('@'));
		}
		return email != null && accountCheck(email);
	}
	
	/*
	 * domain name should have at least 3 alpha-characters in lowercase or numbers
	 */
	public static boolean isValidDomain(String email) {
		if(email.contains("@") && email.contains(".")) {
			email = email.substring(email.indexOf('@'), email.indexOf('.'));
		}
		return email != null && domainCheck(email);
	}
	
	/*
	 * extension name should have at least 2 alpha-characters (no numbers)
	 */
	public static boolean isValidExtension(String email) {
		if(email.contains("@") && email.contains(".")) {
			email = email.substring(email.indexOf('.'));
		}
		return email != null && extCheck(email);
	}
	
	public static boolean accountCheck(String account) {
		if(Character.isDigit(account.charAt(0))) {
			return false;
		} else {
			int count = 0;
			for(char c : account.toCharArray()) {
				if(Character.isLowerCase(c)) {
					count++;
				}
			}
			if(count >= 3) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public static boolean domainCheck(String domain) {
		int count = 0;
		int count2 = 0;
		for(char c : domain.toCharArray()) {
			if(Character.isLowerCase(c)) {
				count++;
			} else if(Character.isDigit(c)) {
				count2++;
			}
		}
		if(count >= 3 || count2 > 0) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public static boolean extCheck(String ext) {
		int count = 0;
		int count2 = 0;
		for(char c : ext.toCharArray()) {
			if(Character.isLowerCase(c)) {
				count++;
			} else if(Character.isDigit(c)) {
				count2++;
			}
		}
		if(count >= 2 && count2 < 1) {
			return true;
		} else {
			return false;
		}
		
	}
}
